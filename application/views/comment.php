<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Comment</h1>

	<div id="body">
	
	<form method="post" action="<?php echo base_url('comment') ?>" class="form-horizontal" novalidate>
                        <div class="form-body">
                            <br>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-5">Search Comment </label>
                                        <div class="col-md-7 controls">
                                             <select class="form-control" name="attribute" id="attribute" size="0" onchange="textField()">
												<option value="">Please Select</option>
												<option value="name">Name</option>
												<option value="created_at">Date</option>
											</select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group row">
                                        
                                        <div class="col-md-7 controls" id="vt">
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>                           

                            

                            <!-- CSRF token -->
                            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />

                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group row">
                                       <div class="col-sm-offset-3 col-sm-5">
                                            <button type="submit" class="btn btn-info btn-rounded btn-sm"><i class="fa fa-save"></i>&nbsp;Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                           
                        </div>
                        
                    </form>
					<br>
		<table class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" border="1"  width="100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Comment</th>
                                    <th>Date</th>
                                </tr>
                            </thead>

                            <tbody>
                            <?php foreach ($comment as $com): 
							   $date = date_create($com['created_at']);
							?>
                                
                                <tr>
                                    <td><?php echo $com['name']; ?></td>
                                    <td><?php echo ucfirst($com['comment']); ?></td>
                                    <td><?php echo date_format($date,"d-m-Y"); ?></td>                                    
                                </tr>

                            <?php endforeach ?>

                            </tbody>


                        </table>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>

<script>
  function textField()
  {
	  var vt = document.getElementById("attribute").value;
	  if(vt == 'name'){
		  document.getElementById("vt").innerHTML = '<input type="text" name="value_type" class="form-control" required>';
	  }else{
		  document.getElementById("vt").innerHTML = '<input type="date" name="value_type" class="form-control" required>';
	  }
  }
</script>